﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
namespace Containment {
    [System.Serializable]
    [CreateAssetMenu(menuName = "Settings/CrimeCodex")]
    public class CrimeCodex : ScriptableObject
    {
        public List<LawStatus> statuses = new List<LawStatus>(); 
        public List<Crime> crimes=new List<Crime>();
        public Dictionary<string, Crime> crimeDict = new Dictionary<string, Crime>();
        public string[] crimeStrings;
        public void Init()
        {
            foreach(var crime in crimes)
            {
                if(!crimeDict.ContainsValue(crime))
                {
                    crimeDict.Add(crime.name, crime);
                }
            }
            Debug.Log(crimeDict.Count);
            crimeStrings = GetCrimes();
            InitLawStatuses();
        }
        string[] GetCrimes()
        {
            return crimeDict.Keys.ToArray();
        }
        public LawStatus GetLawStatus(int severity)
        {
            for (int i = 0; i < statuses.Count; i++)
            {
                if (statuses[i].severityCap <= severity)
                    continue;
                else
                    return statuses[i];
            }
            return statuses.Last();
        }
        void InitLawStatuses()
        {
            for (int i = 0; i < statuses.Count; i++)
            {
                if (i - 1 >= 0)
                    statuses[i].Previous = statuses[i - 1];
                if (i + 1 < statuses.Count)
                    statuses[i].Next = statuses[i + 1];
            }
        }
    }
    [System.Serializable]
    public class LawStatus
    {
        public string name;
        public int severityCap;
        public LawStatus Previous, Next;
    }
}